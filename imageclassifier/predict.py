import os
import argparse
import json
from collections import OrderedDict

import numpy as np

# custom
from imageclassifier.model_setup import load_checkpoint
from imageclassifier.image_process import process_image
import imageclassifier.model_setup
import torch
import torchvision

import logging


def setupLogging():

    #logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
    handler = logging.StreamHandler()

    # handler.setFormatter(formatter)
    root = logging.getLogger()
    root.setLevel(os.environ.get("LOGLEVEL", "INFO"))
    root.addHandler(handler)
    pass


def setupPredictArgparse():
    # Get the command line input into the scripts
    parser = argparse.ArgumentParser()

    parser.add_argument('image_path',
                        action='store',
                        help='Path to image')

    parser.add_argument('checkpoint',
                        action='store',
                        # required=False,
                        default='.',
                        help='Directory of saved checkpoints')

    parser.add_argument('--top_k',
                        action='store',
                        default=5,
                        type=int,
                        dest='top_k',
                        help='Return top KK most likely classes')

    # Use a mapping of categories to real names: python predict.py input checkpoint --category_names cat_to_name.json
    parser.add_argument('--category_names',
                        action='store',
                        default='data/cat_to_name.json',
                        type=argparse.FileType('r'),
                        dest='category_names',
                        help='File name of the mapping of categories to real names')

    parser.add_argument('--gpu',
                        action='store_true',
                        default=False,
                        # type=bool,
                        dest='gpu',
                        help='Use GPU for inference, set a switch to true')

    return parser


def predict(image_path, model, topk=5, gpu=False):
    ''' Predict the class (or classes) of an image using a trained deep learning model.
    '''

    if gpu is True:
        # model.to(device)
        model.cuda()

    # Set model to evaluate
    model.eval()

    img_torch = process_image(image_path)

    img_torch.unsqueeze_(0)
    img_torch = img_torch.float()

    with torch.no_grad():

        if gpu is True:
            img_torch.cuda()

        output = model.forward(img_torch)
        ps = torch.exp(output)  # get the class probabilities from log-softmax

        probs, indices = torch.topk(ps, topk)
        probs = [float(prob) for prob in probs[0]]
        inv_map = {v: k for k, v in model.class_to_idx.items()}
        classes = [inv_map[int(index)] for index in indices[0]]

    return probs, classes


def main():

    setupLogging()

    mainLog = logging.getLogger()

    mainLog.info("Image Classifier Prediction Tool\n")

    parser = setupPredictArgparse()

    parse_results = parser.parse_args()

    #device = torch.device("cuda:0" if parse_results.gpu else "cpu")

    model, optimizer, criterion = load_checkpoint(parse_results.checkpoint)

    if parse_results.gpu:
        model.cuda()

    image_path = os.path.join(parse_results.image_path)

    cat_to_name = json.load(parse_results.category_names)

    # Set up title
    data_num = image_path.split('/')[-2]
    displayTitle = cat_to_name[data_num]

    # print(displayTitle)

    # Load image
    #img = process_image(image_path)

    # Make prediction
    probabilityValues, classes = predict(
        image_path, model, parse_results.top_k, parse_results.gpu)

    #probabilityValues = np.array(probabilities[0][0].cpu())
    categoryNames = [cat_to_name[index] for index in classes]

    # print(categoryNames)
    # print(probabilityValues)

    mainLog.info("")

    mainLog.info("Prediction Results\n")

    row_format = "{:<22}{:<10f}"

    mainLog.info("{:<22}{:<10}".format("Category Name", "Probability Value"))

    for name, probValue in zip(categoryNames, probabilityValues):
        mainLog.info(row_format.format(name, probValue))
    #imgax = plt.subplot(2,1,2)

    # Plot bar chart
    #plt.figure(figsize = (6,10))
    #ax = plt.subplot(2,1,2)


if __name__ == "__main__":
    main()
