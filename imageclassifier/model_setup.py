import enum
import json
import logging
import os
import time
import warnings
from collections import OrderedDict

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
from torch.optim import lr_scheduler
from torchvision import datasets, models, transforms

locallog = logging.getLogger(__name__)


class ModelEnum(enum.IntEnum):
    DENSENET = 1
    VGG = 2
    #RESNET = 3
    ALEXNET = 4

    def __str__(self):
        return self.name.lower()

    def __repr__(self):
        return str(self)

    @classmethod
    def has_value(cls, value):
        return value in cls._value2member_map_

    @staticmethod
    def from_string(s):
        try:
            return ModelEnum[s.upper()]
        except KeyError:
            raise ValueError()


def setupNN(modelType, dropout=0.1, hidden_layer1=512, lr=0.001, gpu=False):
    """
    """

    if not ModelEnum.has_value(modelType.value):
        raise Exception('Model not currently supported ' + modelType)

    # these are functions, not classes
    # https://pytorch.org/docs/stable/torchvision/models.html
    supportedModels = {
        ModelEnum.DENSENET: models.densenet121,
        ModelEnum.VGG: models.vgg16,
        # ModelEnum.RESNET: models.resnet18,
        ModelEnum.ALEXNET: models.alexnet
    }

    if modelType not in supportedModels:
        raise Exception('Model not currently supported ' + modelType)

    selectedModelType = supportedModels[modelType]

    # intialize model using lookup from enum to function
    model = selectedModelType(pretrained=True)
    model.name = modelType.name

    # feature params
    structures = {ModelEnum.VGG: 25088,
                  ModelEnum.DENSENET: 1024,
                  ModelEnum.ALEXNET: 9216,
                  # ModelEnum.RESNET: 1024,
                  }

    # Freeze parameters so we don't backprop through them
    for param in model.parameters():
        param.requires_grad = False

    inputFeatures = structures[modelType]

    classifier = nn.Sequential(OrderedDict([
        ('dropout', nn.Dropout(dropout)),
        ('inputs', nn.Linear(inputFeatures, hidden_layer1)),
        ('relu1', nn.ReLU()),
        ('dropout2', nn.Dropout(dropout)),
        ('fc2', nn.Linear(hidden_layer1, 102)),
        ('output', nn.LogSoftmax(dim=1))
    ]))

    model.classifier = classifier

    # Define loss and optimizer
    criterion = nn.NLLLoss()
    optimizer = optim.Adam(model.classifier.parameters(), lr)

    if gpu:
        model.cuda()

    return model, optimizer, criterion

# Write a function that loads a checkpoint and rebuilds the model


def load_checkpoint(inFile):
    """
    Loads deep learning model checkpoint.
    """

    loadModel = None

    # Load the saved file
    checkpoint = torch.load(inFile, map_location='cpu')

    modelTypeEnum = checkpoint['modelTypeEnum']

    locallog.info("Model Type Loaded:")
    locallog.info(modelTypeEnum)

    loadModel, optimizer, criterion = setupNN(
        modelTypeEnum)  # , DEFAULT_DROP_RATE ,hidden_layer1

    loadModel.class_to_idx = checkpoint['class_to_idx']

    loadModel.load_state_dict(checkpoint['state_dict'])

    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])

    return loadModel, optimizer, criterion


def save_checkpoint(outFile, model, optimizer, trainingImages, modelType, epochs):
    model.class_to_idx = trainingImages.class_to_idx
    model.to('cpu')

    checkpoint = {'architecture': model.name,
                  'classifier': model.classifier,
                  'class_to_idx': model.class_to_idx,
                  'state_dict': model.state_dict(),
                  'modelTypeEnum': modelType,
                  'optimizer_state_dict': optimizer.state_dict(),
                  'epochs': epochs

                  }

    # supress "Couldn't retrieve source code for container of
    # type Linear,RelU, LogSoftmax etc serialization warnings from torch/serialization.py
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        torch.save(checkpoint, outFile)
