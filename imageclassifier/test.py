import argparse
import logging
import os
import sys
import warnings

import torch.cuda

from imageclassifier.data_loader import setupDataLoader
from imageclassifier.model_setup import ModelEnum, load_checkpoint, setupNN
from imageclassifier.train_utils import TrainingUtils


def setupLogging():

    handler = logging.StreamHandler()

    # handler.setFormatter(formatter)
    root = logging.getLogger()
    root.setLevel(os.environ.get("LOGLEVEL", "INFO"))
    root.addHandler(handler)
    pass


def setupTestArgparse():

    parser = argparse.ArgumentParser()

    parser.add_argument('data_directory',
                        action='store',
                        default='/mnt/data/deeplearning/',
                        #required = True,
                        help='Directory to load training data')

    parser.add_argument('--gpu', action='store_true',
                        default=False,
                        dest='gpu',
                        help='Enable GPU for training')

    parser.add_argument('checkpoint',
                        action='store',
                        # required=False,
                        default='.',
                        help='Directory of saved checkpoints')

    return parser


def main():

    try:

        setupLogging()

        mainLog = logging.getLogger()

        parser = setupTestArgparse()

        parse_results = parser.parse_args()

        mainLog.info("Image Classifier Test Tool\n")

        model, optimizer, criterion = load_checkpoint(parse_results.checkpoint)

        #mainLog.info("Arch: " + str(parse_results.arch))
        #mainLog.info("Dropout: " + str(defaultDropOut))
        #mainLog.info("Hidden Units: " + str(parse_results.hidden_units))
        #mainLog.info("Learning Rate: " + str(parse_results.learning_rate))

        if parse_results.gpu:
            model.cuda()

        if parse_results.gpu and not torch.cuda.is_available():
            logging.error("GPU requested however not available")
            exit(1)

        if parse_results.gpu:
            model = model.cuda()

        image_datasets, dataloaders = setupDataLoader(
            parse_results.data_directory)

        trainUtils = TrainingUtils(parse_results.gpu)

        mainLog.info("Training Model")
        trainUtils.testNetwork(model, dataloaders, optimizer, criterion)

        mainLog.info("")

    except KeyboardInterrupt:
        mainLog.info("Training aborted by user")
        pass


if __name__ == "__main__":
    main()
