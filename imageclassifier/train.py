import argparse
import logging
import os
import sys
import time
import warnings

import torch.cuda

from imageclassifier.data_loader import setupDataLoader
from imageclassifier.model_setup import ModelEnum, save_checkpoint, setupNN
from imageclassifier.train_utils import TrainingUtils


def setupLogging():

    handler = logging.StreamHandler()

    root = logging.getLogger()
    root.setLevel(os.environ.get("LOGLEVEL", "INFO"))
    root.addHandler(handler)
    pass


def setupTrainArgparse():

    parser = argparse.ArgumentParser()

    parser.add_argument('data_directory',
                        action='store',
                        default='/mnt/data/deeplearning/',
                        #required = True,
                        help='Directory to load training data')

    parser.add_argument('--save_dir',
                        action='store',
                        default='.',
                        dest='save_dir',
                        help='Set directory to save checkpoints')

    parser.add_argument('--arch',
                        action='store',
                        default=ModelEnum.DENSENET,
                        dest='arch',
                        choices=list(ModelEnum),
                        type=ModelEnum.from_string,
                        help='architecture')

    # Set hyperparameters
    parser.add_argument('--learning_rate',
                        action='store',
                        default=0.001,
                        dest='learning_rate',
                        type=float,
                        help='learning rate')

    parser.add_argument('--hidden_units', action='store',
                        default=512,
                        dest='hidden_units',
                        type=int,
                        help='hidden units')

    parser.add_argument('--epochs', action='store',
                        default=11,
                        dest='epochs',
                        type=int,
                        help='number of epochs')

    parser.add_argument('--gpu', action='store_true',
                        default=False,
                        dest='gpu',
                        help='Enable GPU for training')

    return parser


def main():

    try:

        setupLogging()

        mainLog = logging.getLogger()

        parser = setupTrainArgparse()

        parse_results = parser.parse_args()

        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        if parse_results.gpu and not torch.cuda.is_available():
            logging.error("GPU requested however not available")
            exit(1)

        defaultDropOut = 0.1

        mainLog.info("Image Classifier Training Tool\n")

        mainLog.info("Arch: " + str(parse_results.arch))
        mainLog.info("Dropout: " + str(defaultDropOut))
        mainLog.info("Hidden Units: " + str(parse_results.hidden_units))
        mainLog.info("Learning Rate: " + str(parse_results.learning_rate))

        if not os.path.exists(parse_results.save_dir):
            os.mkdir(parse_results.save_dir)

        if parse_results.gpu:

            mainLog.info("CUDA Enabled")
            torch.cuda.set_device("cuda:0")

        model, optimizer, criterion = setupNN(parse_results.arch, defaultDropOut,
                                              parse_results.hidden_units, parse_results.learning_rate,
                                              parse_results.gpu)

        if parse_results.gpu:
            model.cuda()

        image_datasets, dataloaders = setupDataLoader(
            parse_results.data_directory)

        trainUtils = TrainingUtils(parse_results.gpu)

        mainLog.info("Training Model")

        start = time.time()

        trainUtils.trainModel(model, dataloaders, optimizer,
                              criterion, parse_results.epochs, 40)

        end = time.time()
        trainTime = (end - start)
        mainLog.info("Training time elapsed in seconds")
        mainLog.info(trainTime)

        # todo generate file name
        outFile = str.format("checkpoint-{arch}-{epochs}.pth",
                             arch=str(parse_results.arch), epochs=str(parse_results.epochs))

        mainLog.info("\nSaving checkpoint " + outFile)

        checkpointFile = os.path.join(parse_results.save_dir, outFile)

        # todo, save additional hyper parameters.  Would like to encapsulate them all in a class first
        save_checkpoint(checkpointFile, model, optimizer,
                        image_datasets['train'], parse_results.arch, parse_results.epochs)

        mainLog.info("")

    except KeyboardInterrupt:
        mainLog.info("Training aborted by user")
        pass


if __name__ == "__main__":
    main()
