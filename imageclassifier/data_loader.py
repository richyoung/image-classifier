import copy
import enum
import json
import os
import time
from collections import OrderedDict

import matplotlib.pyplot as plt
import numpy as np
import PIL
import seaborn as sns

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
from torch.optim import lr_scheduler
from torchvision import datasets, models, transforms


def setupDataLoader(dataDir):

    #base_data_dir = baseDir

    data_dir = os.path.join(dataDir)

    # Define your transforms for the training, validation, and testing sets
    data_transforms = {
        'train': transforms.Compose([
            transforms.RandomRotation(30),
            transforms.RandomResizedCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406],
                                 [0.229, 0.224, 0.225])
        ]),
        'valid': transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406],
                                 [0.229, 0.224, 0.225])
        ]),
        'test': transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406],
                                 [0.229, 0.224, 0.225])
        ]),
    }

    # Load the datasets with ImageFolder

    image_datasets = {x: datasets.ImageFolder(os.path.join(
        data_dir, x), data_transforms[x]) for x in ['train', 'valid', 'test']}

    class_names = image_datasets['train'].classes
    dataset_sizes = {x: len(image_datasets[x])
                     for x in ['train', 'valid', 'test']}

    # Using the image datasets and the tranforms, define the dataloaders
    dataloaders = {}
    dataloaders['train'] = torch.utils.data.DataLoader(
        image_datasets['train'], batch_size=32, shuffle=True)
    dataloaders['valid'] = torch.utils.data.DataLoader(
        image_datasets['valid'], batch_size=32)
    dataloaders['test'] = torch.utils.data.DataLoader(
        image_datasets['test'], batch_size=32)

    return image_datasets, dataloaders
