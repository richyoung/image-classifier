import copy
import enum
import json
import os
import time
from collections import OrderedDict

import matplotlib.pyplot as plt
import numpy as np
import PIL
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
from torch.optim import lr_scheduler
from torchvision import datasets, models, transforms


class TrainingUtils:

    def __init__(self, useGPU=False):

        self._useGPU = useGPU

        pass

    def validation(self, model, optimizer, testloader, criterion):
        test_loss = 0
        accuracy = 0

        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        for ii, (inputs, labels) in enumerate(testloader):

            optimizer.zero_grad()

            if self._useGPU:
                inputs, labels = inputs.cuda(), labels.cuda()
                #inputs, labels = inputs.to(device), labels.to(device)

            output = model.forward(inputs)
            test_loss += criterion(output, labels).item()

            ps = torch.exp(output)
            equality = (labels.data == ps.max(dim=1)[1])
            accuracy += equality.type(torch.FloatTensor).mean()

        return test_loss, accuracy

    def trainModel(self, model, dataloaders, optimizer, criterion, epochs, print_every):

        steps = 0

        # todo
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        for e in range(epochs):
            running_loss = 0
            model.train()  # drop out enabled

            for ii, (inputs, labels) in enumerate(dataloaders['train']):
                steps += 1

                if self._useGPU:
                    inputs, labels = inputs.cuda(), labels.cuda()
                    #inputs, labels = inputs.to(device), labels.to(device)

                optimizer.zero_grad()

                # Forward and backward passes
                outputs = model.forward(inputs)
                loss = criterion(outputs, labels)

                # failed with resnet
                loss.backward()

                optimizer.step()

                running_loss += loss.item()

                if steps % print_every == 0:
                    model.eval()  # Make sure network is in eval mode for inference
                    vlost = 0
                    accuracy = 0

                    with torch.no_grad():
                        valid_loss, accuracy = self.validation(
                            model, optimizer, dataloaders['valid'], criterion)

                    print("Epoch: {}/{}... ".format(e+1, epochs),
                          "Training Loss: {:.4f}".format(
                              running_loss/print_every),
                          "Validation Lost {:.4f}".format(
                              valid_loss / len(dataloaders['valid'])),
                          "Validation Accuracy: {:.4f}".format(accuracy / len(dataloaders['valid'])))

                    running_loss = 0

    def testNetwork(self, model, dataloaders, optimizer,  criterion):

        with torch.no_grad():
            valid_loss, accuracy = self.validation(
                model, optimizer, dataloaders['test'], criterion)

            print('Accuracy of the network on the 10000 test images: %d %%' %
                  (100 * (accuracy / len(dataloaders['test']))))
