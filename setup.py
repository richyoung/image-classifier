import re
import logging
from setuptools import setup, find_packages

setup(
    name="imageclassifier",
    version="1.0",
    packages=find_packages(exclude=["tests"]),
    install_requires=[],
    entry_points = {
        'console_scripts':
        [
            #Prefix to be more specific for CLI installs
            'img-predict=imageclassifier.predict:main',
            'img-train=imageclassifier.train:main',
            'img-test=imageclassifier.test:main',
        ],
    },
    data_files = [("", ["LICENSE"])]
    #scripts=['bin/example']
)
